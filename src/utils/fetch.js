import axios from 'axios';

const BASE_URL = 'https://dev-api.bisnisblanja.my.id/api';
const HEADERS = {
  'x-channel': 'EOdZJwnM60re4xvRXaoEG3lKODqjBYp9z52AgVP1LDkmW7ybQ3',
};
// const HEADERS_BLANJA = {
//   'x-channel': 'WRkZ6oOWj0d2BbyzJ4egmYVKDdK9nvGMQ3xwAD7P1LrElX5ap5',
// };

const fetch = (url, method, param1, param2) => new Promise((resolve, reject) => {
  axios[method](url, param1, param2)
    .then((res) => resolve(res.data))
    .catch((err) => {
      const defaultError = {
        code: 500,
        status: 'error',
        message: 'Failed to fetch data. Please contact developer.',
      };

      if (!err.response) {
        reject(defaultError);
      } else if (!err.response.data) {
        reject(defaultError);
      } else {
        reject(err.response.data);
      }
    });
});

export const areaCities = (id) => (
  fetch(`${BASE_URL}/v1/catalog/provinces/${id}/cities?limit=99`, 'get', { headers: HEADERS, })
);

export const areaProvinces = () => (
  fetch(`${BASE_URL}/v1/catalog/provinces?limit=99`, 'get', { headers: HEADERS, })
);

export const areaSubdistricts = (id1, id2) => (
  fetch(`${BASE_URL}/v1/catalog/provinces/${id1}/cities/${id2}/subdistricts?limit=99`, 'get', { headers: HEADERS })
);

export const areaVillages = (id1, id2, id3) => (
  fetch(`${BASE_URL}/v1/catalog/provinces/${id1}/cities/${id2}/subdistricts/${id3}/villages?limit=99`, 'get', { headers: HEADERS })
);

export const categories = () => (
  fetch(`${BASE_URL}/v1/catalog/categories`, 'get', { headers: HEADERS })
);

export const document = async (data) => (
  fetch(`${BASE_URL}/v1/landing/files/upload`, 'post', data, { headers: HEADERS, })
);

export const fetchUserData = (data) => (
  fetch(`${BASE_URL}/v1/me`, 'get', { headers: { ...HEADERS, Authorization: data, }, })
);

export const fetchCategoryData = () => (
  fetch(`${BASE_URL}/v1/catalog/categories`, 'get', { headers: HEADERS })
);

export const homeAds = async () => (
  fetch(`${BASE_URL}/v1/banner/small-main`, 'get', { headers: HEADERS })
);

export const homeBanner = async () => (
  fetch(`${BASE_URL}/v1/banner/main`, 'get', { headers: HEADERS })
);

export const productDetail = async (id) => (
  fetch(`${BASE_URL}/v1/catalog/products/${id}`, 'get', { headers: HEADERS, })
);

export const homeCategories = async () => (
  fetch(`${BASE_URL}/v1/banner/categories`, 'get', { headers: HEADERS })
);

export const homeProducts = (id) => (
  fetch(`${BASE_URL}/v1/banner/products-by-sku?sku=${id}`, 'get', { headers: HEADERS })
);

export const loginUser = data => (
  fetch(`${BASE_URL}/v1/auth/login`, 'post', data, { headers: HEADERS })
);

export const registerCompany = async (data) => (
  fetch(`${BASE_URL}/v1/auth/register/company`, 'post', data, { headers: HEADERS, })
);

export const postOrderCompany = async ({ token, data }) => (
  fetch(`${BASE_URL}/v1/company/orders`, 'post', data,
    { headers: { ...HEADERS, Authorization: token, }, })
);
