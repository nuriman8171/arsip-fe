import React from 'react';
import { useParams } from 'react-router-dom';
import Button from '../../components/elements/Button';
import ProfileForm from '../../components/forms/Profile';
import Snackbar from '../../components/elements/Snackbar';
import styles from './styles.scoped.css';

export default function Profile() {
  const { pages } = useParams();

  const detail = (label, val) => (
    <div className={styles.detail}>
      <label>{label}</label>
      <p>{val}</p>
    </div>
  );
  return (
    <section className={styles.root}>
      <header>
        <h4>Pengaturan Profile</h4>
        {pages !== 'edit' && <Button onClick={() => history.push('/profile/edit')}>Edit Profile</Button>}
      </header>
      <article>
        <form>
          <figure>
            <img alt="broken-image" src={"/assets/img-empty.svg"} />
          </figure>
          <form>
            <p>Informasi Dasar</p>
            {detail('Nama',)}
            <section>
              {detail('No Hp',)}
              {detail('Email',)}
            </section>
            {detail('Role', )}
            {detail('Alamat Perusahaan',)}
            
          <DetailDocument/>
          </form>
        </form>
      </article>
    </section>
  );
}

export function ProfileDetail() {

  const detail = (label, val) => (
    <div className={styles.detail}>
      <label>{label}</label>
      <p>{val}</p>
    </div>
  );
  return (<div>
    <figure>
      <img alt="logo" src={'/assets/img-empty.svg'} />
    </figure>
    <article>
      <p>Informasi Dasar</p>
      {detail('Nama Perusahaan')}
      <section>
        {detail('Nama Person in Charge')}
        {/* {detail('Jabatan')} */}
      </section>
      {/* {detail('Deskripsi Perusahaan')}
      {detail('Alamat Perusahaan', `, Kel.,
      Kec., Kota`)} */}
      {detail('Alamat Email')}
      {detail('Nomor Telepon')}
    </article>
  </div>);
}

export function DetailDocument() {
  const document = [
    { name: ' KTP', }, { name: ' NIB', },
    { name: ' NPWP', }, { name: ' SIUP', }
  ];
  const status = document.filter(i => !i.value);
  const lastStatus = status.pop();

  const docs = (title, label, url) => (
    <div className={styles.docs}>
      <p>{title}</p>
      <label className={url ? styles.true : styles.false}>{label}</label>
      {url &&
        <a href={url} rel="noopener" target="_blank">
          <img alt="docs" src="/assets/ic-download.svg" />
          {title}
        </a>
      }
    </div >
  );

  return (
    <div className={styles.document}>
      <p>Kelengkapan Dokumen Perusahaan</p>
      {lastStatus?.value === null ?
        <Snackbar show variant="warning">
          {`Anda belum Mengunggah ${status.map(i => i.name) || ''} ${status.length !== 0 ? 'dan juga' : ''}
          ${lastStatus.name || ''} untuk bisa transaksi jual beli`}
        </Snackbar> :
        <Snackbar show variant="success">
          Anda sudah bisa melakukan transaksi jual beli!
        </Snackbar>
      }
      <section>
        {docs('KTP', '(Anda sudah mengunggah KTP)')}
        {docs('NIB', '(Anda sudah mengunggah NIB)')}
        {docs('NPWP', '(Anda sudah mengunggah NPWP)')}
        {docs('SIUP', '(Anda sudah mengunggah SIUP)')}
      </section>
    </div>
  );
}


