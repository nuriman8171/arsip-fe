import { resetMessage, fetchArea } from './actions';
import { FAILED, LOADING, DATA_FETCHED } from './constants';

const initialState = {
  resetMessage,
  fetchArea,
  isLoading: {
    submit: false,
    provinces: false,
    cities: false,
    subdistricts: false,
    villages: false,
  },
  message: '',
  provinces: [],
  cities: [],
  subdistricts: [],
  villages: [],
};

export default function reducer(state = initialState, action = {}) {
  const { type, isLoading, message, key, data } = action;

  switch (type) {
    case DATA_FETCHED:
      return {
        ...state,
        isLoading: { ...state.isLoading, [key]: false },
        [key]: data,
      };
    case FAILED:
      return {
        ...state,
        message,
        isLoading: { ...state.isLoading, submit: false }
      };
    case LOADING:
      return {
        ...state,
        isLoading
      };
    default:
      return state;
  }
}
