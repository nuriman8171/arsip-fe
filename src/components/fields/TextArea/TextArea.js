import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.scoped.css';

export default function TextArea(props) {
  const { className, input, meta, rows, textAreaProps } = props;
  const { dirty, error, touched } = meta;
  const classes = [
    styles.root,
    textAreaProps.required && styles.required,
    !!error && (dirty || touched) && styles.error,
    className
  ].filter(Boolean).join(' ');

  return (
    <div className={classes}>
      <label htmlFor={input.name}>
        {textAreaProps.label}
        {textAreaProps.required && <span>*</span>}
      </label>
      <textarea id={input.name} rows={rows} {...input} {...textAreaProps}
      />
      {!!error && (dirty || touched) && <small>{error}</small>}
    </div>
  );
}

TextArea.defaultProps = {
  className: '',
  input: {},
  meta: {},
  rows: 4,
  textAreaProps: {},
};

TextArea.propTypes = {
  className: PropTypes.string,
  input: PropTypes.object,
  meta: PropTypes.object,
  rows: PropTypes.number,
  textAreaProps: PropTypes.object,
};
