import validate from '../validate';

jest.mock('../../../../utils/validation', () => ({
  isAreaCode: v => v === '021',
  isEmail: v => v === 'test@abc.co.id',
  isMobilePhone: v => v === '08123456789',
  isPassword: v => v === '12345678',
  isPhone: v => v === '1234567',
}));

describe('src/components/forms/Register/validate', () => {
  test('validate', () => {
    const input1 = {
      email: 'test',
      mobile_number: 'abc',
      password: '1234',
      password_confirmation: '1234',
    };
    expect(validate(input1)).toMatchObject({
      company_name: 'Harus diisi!',
      person_in_charge: 'Harus diisi!',
      email: 'Mohon memasukkan email yang benar',
      mobile_number: 'Mohon memasukkan no. handphone yang benar',
      password: 'Minimal terdiri 8 karakter',
      password_confirmation: 'Minimal terdiri 8 karakter',
    });

    const input3 = {
      company_name: 'test',
      person_in_charge: 'ceo',
      email: 'test@abc.co.id',
      mobile_number: '08123456789',
      password: '12345678',
      password_confirmation: '12345678',
      file: { name: 'img.jpg', size: 60000 }
    };
    expect(validate(input3)).toMatchObject({
      company_name: '',
      person_in_charge: '',
      email: '',
      mobile_number: '',
      password: '',
      password_confirmation: '',
      file: ''
    });

    const input4 = { file: { size: 6000000 } };
    expect(validate(input4)).toMatchObject({
      file: 'File yang anda upload lebih dari 5MB',
    });

    const input5 = { file: { name: 'test.pdf' } };
    expect(validate(input5)).toMatchObject({
      file: 'File yang anda upload tidak dalam format jpeg/png',
    });
  });
});
