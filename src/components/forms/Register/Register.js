import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { Field } from 'redux-form';
import Button from '../../elements/Button';
import TextField from '../../fields/Text';
import styles from './styles.scoped.css';

export default function Register(props) {
  const { isLoading } = useSelector(s => s.register);
  const { handleSubmit, invalid } = props;
  const [show, setShow] = useState({ password: false, confirmPassword: false });
  const changeType = (i) => () => setShow({ ...show, [i]: !show[i] });
  const textProps = [
    { label: 'Nama', placeholder: 'Isikan nama perusahaan Anda', required: true },
    { label: 'Email', placeholder: 'Isikan Email Anda', type: 'email', required: true },
    { label: 'No. Handphone', placeholder: 'Isikan nomor handphone Anda', type: 'tel', required: true },
    {
      label: 'Password',
      icon: show.password ? 'password-hide' : 'password-show',
      placeholder: 'Password',
      type: show.password ? 'text' : 'password',
      required: true
    },
    {
      label: 'Confirm Password',
      icon: show.confirmPassword ? 'password-hide' : 'password-show',
      placeholder: 'Confirm Password',
      type: show.confirmPassword ? 'text' : 'password',
      required: true
    },
  ];

  const btnDisabled = invalid || isLoading.register;

  return (
    <form className={styles.root} onSubmit={handleSubmit}>
      <h4>Daftarkan Akun Anda</h4>
      <Field component={TextField} inputProps={textProps[0]} name="name" />
      <Field component={TextField} inputProps={textProps[1]} name="email" />
      <Field component={TextField} inputProps={textProps[2]} name="mobile_number" />
      <Field component={TextField} inputProps={textProps[3]} name="password" onClickIcon={changeType('password')} />
      <Field component={TextField} inputProps={textProps[4]} name="password_confirmation" onClickIcon={changeType('confirmPassword')} />
      <Button disabled={btnDisabled} fixed isLoading={isLoading} size="large" type="submit">Daftar</Button>
    </form>
  );
}

Register.defaultProps = {
  handleSubmit: () => { },
  invalid: true,
};

Register.propTypes = {
  handleSubmit: PropTypes.func,
  invalid: PropTypes.bool,
};
