import { isEmail, isMobilePhone, isPassword } from '../../../utils/validation';

export default function validate(values) {
  const { company_name, person_in_charge, email, mobile_number,
    password, password_confirmation, file } = values;
  const required = 'Harus diisi!';

  return {
    company_name: !company_name ? required : '',
    person_in_charge: !person_in_charge ? required : '',
    email: !isEmail(email) ? 'Mohon memasukkan email yang benar' : '',
    mobile_number: !isMobilePhone(mobile_number) ? 'Mohon memasukkan no. handphone yang benar' : '',
    password: !isPassword(password) ? 'Minimal terdiri 8 karakter' : '',
    password_confirmation: !isPassword(password_confirmation) ? 'Minimal terdiri 8 karakter' : '',
    file: ((v) => {
      if (!v) {
        return required;
      }
      if (v.size > 5 * Math.pow(1024, 2)) {
        return 'File yang anda upload lebih dari 5MB';
      }
      if (!/\.(jpe?g|png)$/i.test(v.name)) {
        return 'File yang anda upload tidak dalam format jpeg/png';
      }
      return '';
    })(file)
  };
}
