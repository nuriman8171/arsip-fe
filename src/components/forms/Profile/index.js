import { reduxForm } from 'redux-form';
import Component from './Profile';
import validate from './validate';

export default reduxForm({
  form: 'profile',
  validate
})(Component);
