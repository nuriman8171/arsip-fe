import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { Field } from 'redux-form';
import SelectField from '../../fields/Select';
import TextField from '../../fields/Text';
import styles from './styles.scoped.css';
import Button from '../../elements/Button';

export default function Address(props) {
  const { change, initial, initialize, pristine, touch, invalid,
    handleSubmit, onClose, onPreview } = props;
  const dispatch = useDispatch();
  const { cities, provinces, subdistricts, villages, fetchArea } = useSelector(s => s.profile);
  const formData = useSelector(s => s.form.address.values);
  const { store_city_id, store_province_id, store_subdistrict_id,
    store_village_id, store_address } = formData || {};
  const textProps = { label: 'Alamat Perusahaan', placeholder: 'Isikan alamat perusahaan', required: true };
  const selectProps = [
    { label: 'Provinsi', placeholder: 'Pilih provinsi alamat perusahaan', required: true },
    { label: 'Kota / Kabupaten', placeholder: 'Pilih kota / kabupaten alamat perusahaan', required: true },
    { label: 'Kecamatan', placeholder: 'Pilih kecamatan alamat perusahaan', required: true },
    { label: 'Kelurahan / Desa', placeholder: 'Pilih kelurahan / desa alamat perusahaan', required: true },
  ];

  const [mapProvinces] = useMappedAddress(provinces);
  const [mapCities] = useMappedAddress(cities);
  const [mapSubdistricts, resetSubdistricts] = useMappedAddress(subdistricts);
  const [mapVillages, resetVillages] = useMappedAddress(villages);

  useEffect(() => {
    touch('store_city_id', 'store_subdistrict_id', 'store_village_id');
    dispatch(fetchArea('provinces'));
  }, []);

  useEffect(() => {
    store_province_id && dispatch(fetchArea('cities', store_province_id));
    change('store_city_id', '');
    change('store_subdistrict_id', '');
    change('store_village_id', '');
    resetSubdistricts();
    resetVillages();
  }, [store_province_id]);

  useEffect(() => {
    store_city_id && dispatch(fetchArea('subdistricts', store_province_id, store_city_id));
    change('store_subdistrict_id', '');
    change('store_village_id', '');
    resetVillages();
  }, [store_city_id]);

  useEffect(() => {
    store_subdistrict_id && dispatch(fetchArea('villages', store_province_id, store_city_id, store_subdistrict_id));
    change('store_village_id', '');
    (initial.store_village_id && pristine) && initialize(initial);
  }, [store_subdistrict_id]);

  useEffect(function () {
    if(store_village_id) {
      const provinceName = provinces.find(i => i.id===Number(store_province_id))?.name;
      const cityName = cities.find(i => i.id===Number(store_city_id))?.name;
      const subdistrictName = subdistricts.find(i => i.id===Number(store_subdistrict_id))?.name;
      const villageName = villages.find(i => i.id===Number(store_village_id))?.name;
      const fullAddress = `${store_address}, Kel. ${villageName}, Kec. ${subdistrictName},
Kota ${cityName}, ${provinceName}`;
      villageName&&onPreview(fullAddress);
    }
  }, [store_village_id, store_address]);

  return (
    <form className={styles.root} onSubmit={handleSubmit}>
      <Field component={TextField} inputProps={textProps} name="store_address" />
      <Field component={SelectField} name="store_province_id" options={mapProvinces} selectProps={selectProps[0]} />
      <Field component={SelectField} name="store_city_id" options={mapCities} selectProps={selectProps[1]} />
      <Field component={SelectField} name="store_subdistrict_id" options={mapSubdistricts} selectProps={selectProps[2]} />
      <Field component={SelectField} name="store_village_id" options={mapVillages} selectProps={selectProps[3]} />
      <footer>
        <Button fixed onClick={onClose} size="large" variant="ghost">Batal</Button>
        <Button disabled={invalid} fixed size="large" type="submit">Simpan!</Button>
      </footer>
    </form>
  );
}

Address.defaultProps = {
  change: () => { },
  formName: '',
  handleSubmit: () => { },
  initial: {},
  initialize: () => { },
  invalid: true,
  onClose: () => { },
  onPreview: () => { },
  pristine: true,
  touch: () => { },
};

Address.propTypes = {
  change: PropTypes.func,
  formName: PropTypes.string,
  handleSubmit: PropTypes.func,
  initial: PropTypes.object,
  initialize: PropTypes.func,
  invalid: PropTypes.bool,
  onClose: PropTypes.func,
  onPreview: PropTypes.func,
  pristine: PropTypes.bool,
  touch: PropTypes.func,
};

export function useMappedAddress(data) {
  const [mapData, setMapData] = useState([]);

  useEffect(() => {
    setMapData(data.map(i => ({ label: i.name, value: i.id })));
  }, [data]);

  return [mapData, () => setMapData([])];
}
