import validate from '../validate';

describe('src/components/forms/Address/validate', () => {
  test('validate', () => {
    const input1 = {
      store_city_id: '10',
      store_province_id: '2',
      store_subdistrict_id: '59',
      store_village_id: '717',
      store_address: 'asdasdasd'
    };
    expect(validate(input1)).toMatchObject({
      store_city_id: '',
      store_province_id: '',
      store_subdistrict_id: '',
      store_village_id: '',
      store_address: ''
    });

    const input2 = {
      store_address: '',
      store_province_id: '12',
      store_city_id: '',
      store_subdistrict_id: '',
      store_village_id: '',
    };
    expect(validate(input2)).toMatchObject({
      store_address: 'Harus diisi!',
      store_province_id: '',
      store_city_id: 'Harus diisi!',
      store_subdistrict_id: 'Isi kabupaten / kota terlebih dahulu',
      store_village_id: 'Isi kecamatan terlebih dahulu',
    });

    const input3= { store_province_id: '' };
    expect(validate(input3)).toMatchObject({ store_province_id: 'Harus diisi!' });
  });
});
