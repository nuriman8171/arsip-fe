import { reduxForm } from 'redux-form';
import Component from './Address';
import validate from './validate';

export default reduxForm({
  form: 'address',
  validate,
})(Component);
