import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import Toast from '../Toast';

describe('src/components/elements/Toast', () => {
  test('render success status', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Toast status={true}/>);
    expect(tree).toMatchSnapshot();

    const closeIcon = tree.props.children.props.children[0];
    closeIcon.props.onClick();
  });

  test('render failed status', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Toast redirect={'/home'} status={false} />);
    expect(tree).toMatchSnapshot();

    const closeIcon = tree.props.children.props.children[0];
    closeIcon.props.onClick();
  });
});
