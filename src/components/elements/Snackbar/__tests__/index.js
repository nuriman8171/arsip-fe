import React from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import Snackbar from '../Snackbar';

describe('src/components/elements/Snackbar', () => {
  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render( <Snackbar/> );
    expect(tree).toMatchSnapshot();
  });
});
