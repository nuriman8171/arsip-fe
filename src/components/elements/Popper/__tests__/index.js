import React, { useEffect, useState } from 'react';
import ShallowRenderer from 'react-test-renderer/shallow';
import Popper from '../Popper';

describe('src/components/elements/Popper', () => {
  test('render', () => {
    const shallow = new ShallowRenderer();
    const tree = shallow.render(<Popper />);
    Popper.defaultProps.onClose();
    expect(tree).toMatchSnapshot();
  });

  test('set content', () => {
    window.addEventListener = jest.fn();
    const setContent = jest.fn();

    useEffect.mockImplementationOnce(fn => fn());
    useState.mockImplementationOnce(v => [v, setContent]);
    Popper({ ...Popper.defaultProps, open: true, children: 'test' });
    expect(setContent).toHaveBeenCalledWith('test');
    expect(window.addEventListener).toHaveBeenCalled();

    useEffect.mockImplementationOnce(f => f()());
    useState.mockImplementationOnce(v => [v, setContent]);
    Popper({ ...Popper.defaultProps, open: true, children: 'test' });
    expect(setContent).toHaveBeenCalledWith(null);
    expect(window.addEventListener).toHaveBeenCalled();

    const event = { stopPropagation: jest.fn() };
    const result = Popper(Popper.defaultProps);
    result.props.onClick(event);
    expect(event.stopPropagation).toHaveBeenCalled();
  });
});
